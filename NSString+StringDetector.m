//
//  NSString+StringDetector.m
//  SinchIM
//
//  Created by SMIT on 12/8/16.
//
//

#import "NSString+StringDetector.h"


@implementation NSString (StringDetector)


//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void *)stringByFormatingString:(NSString *)replace :(myCompletion) compblock {
//-------------------------------------------------------------------------------------------------------------------------------------------------
    
    
    // ------------------------------------------------------------------------------//
    //FIRST CHECK IF CHAT MESSAGE IS JSON
    // IF YES - > NO NEED TO PARSE
    // IF NO  - > PARSE TO FIND OUT LINK AND PHONE NUMBER
     // ------------------------------------------------------------------------------//
    NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    if (json) {
        compblock(false,@"");
    }else{
        
        NSString *replaced,*jsonLink;
        NSMutableArray *link = [[NSMutableArray alloc]init];
        NSMutableArray *titles = [[NSMutableArray alloc]init];
        
        NSDataDetector *dataDetector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink|NSTextCheckingTypePhoneNumber error:nil];
        
        NSArray *resultArray = [dataDetector matchesInString:self
                                options:0
                                range:NSMakeRange(0,[self length])];
        
        
        for (NSTextCheckingResult *result in resultArray)
        {
            if ([result resultType] == NSTextCheckingTypeLink)
            {
                NSURL *url = [result URL];
                NSLog(@"url:%@",[url description]);
                
                
                if (![self linkValidation:url]) {
                    replaced=  [self  stringByReplacingCharactersInRange:[result range] withString:replace];
                }else{
                    
                    [titles addObject:[self extractTitleFromLink:url]];
                    [link addObject:[url description]];
                }
                
            } else if ([result resultType] == NSTextCheckingTypePhoneNumber) {
                NSString *phoneNumber = [result phoneNumber];
                NSLog(@"tel:%@",phoneNumber);
                
                if ([self phoneNumberValidation:phoneNumber]) {
                    replaced=  [self  stringByReplacingCharactersInRange:[result range] withString:replace];
                }
                
            }
        }
        
        // ------------------------------------------------------------------------------//
        //TO REMOVE LINKS FROM ORIGINAL CHAT MESSAGE
        // ------------------------------------------------------------------------------//
        if (link.count > 0) {
            
            NSString *extractedString =[[NSString alloc]init] ;
            for (NSString *str in link) {
                extractedString = [self stringByReplacingOccurrencesOfString:str withString:@""];
                
            }
            compblock(true,[self generateJSONString:extractedString :link : titles]);
        }
        else if (replaced) {
            compblock(true,[self generateJSONString:replaced]);
        }
        
        else{
            compblock(false,@"");
        }
        
    }
}


//-------------------------------------------------------------------------------------------------------------------------------------------------
-(BOOL)phoneNumberValidation:(NSString *)phoneNumber{
    //-------------------------------------------------------------------------------------------------------------------------------------------------

    if([phoneNumber hasPrefix:@"+"] && phoneNumber.length == 12) {
        return true;
    }
    return false;
}

-(BOOL)linkValidation:(NSURL *)link{
    
    NSString *domain = [link host ];
    
    if ([domain isEqualToString:@"www.carlist.my"]) {
        return true;
    }
    
    return false;
}


//-------------------------------------------------------------------------------------------------------------------------------------------------
-(NSString *)generateJSONString : (NSString *) message {
    //-------------------------------------------------------------------------------------------------------------------------------------------------

    NSError *error;
    
    NSDictionary *dic = [NSDictionary dictionaryWithObject:message forKey:@"message"];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic
                                                       options:NSJSONWritingPrettyPrinted error:&error];
    
    if (! jsonData) {
        NSLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
        return @"{}";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}


//-------------------------------------------------------------------------------------------------------------------------------------------------
-(NSString *)generateJSONString : (NSString *) message :(NSMutableArray *) link : (NSMutableArray *) titles {
    //-------------------------------------------------------------------------------------------------------------------------------------------------

    NSError *error;

    NSMutableDictionary *links = [[NSMutableDictionary alloc]init];
    for (NSString *urlString in link ) {
        NSUInteger index = [link indexOfObject:urlString];
        NSString *stringTitle = [titles objectAtIndex:index];
        [links setDictionary:@{@"url": urlString,@"title":stringTitle}];
        
    }
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:message,@"message",links,@"Links", nil];
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict
                                                       options:NSJSONWritingPrettyPrinted error:&error];
    
    if (! jsonData) {
        NSLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
        return @"{}";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
-(NSString *)extractTitleFromLink : (NSURL *) url{
    //-------------------------------------------------------------------------------------------------------------------------------------------------

    NSLog(@"path components: %@", [url pathComponents]);
    NSString *title =[[url pathComponents][3] stringByReplacingOccurrencesOfString:@"-" withString:@" "];
    
    NSString *priceString = [NSString stringWithFormat:@" - %@",[url pathComponents][2]];
    NSString *domainString = [NSString stringWithFormat:@" - %@",[self extractStringFromHost:[url host]]];
    NSString * str = [title stringByAppendingString:priceString];
    str = [str stringByAppendingString:domainString];
    return str;

    
}


//-------------------------------------------------------------------------------------------------------------------------------------------------
-(NSString *)extractStringFromHost :(NSString *)host {
    //-------------------------------------------------------------------------------------------------------------------------------------------------

    return  [host stringByReplacingOccurrencesOfString:@"www." withString:@""];
    
}



@end
