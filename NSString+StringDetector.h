//
//  NSString+StringDetector.h
//  SinchIM
//
//  Created by SMIT on 12/8/16.
//
//

#import <Foundation/Foundation.h>

typedef void(^myCompletion)(BOOL,NSString *) ;

@interface NSString (StringDetector)

- (void *)stringByFormatingString:(NSString *)replace :(myCompletion) compblock;
-(NSString *)generateJSONString : (NSString *) message;
// test 1

//test 2
// test 3
// test 4


@end
