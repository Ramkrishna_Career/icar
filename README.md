# README #

This README would normally document whatever steps are necessary to get your application up and running.

###  Assignment submission ###

We have a chat functionality in our app. Where consumers can talk to sellers and vice versa. The improve the experience and prevent the integrity of the chat we blacklist certains parts of the chat i.e. presenting links in proper format or preventing sharing of phone numbers.
You need to make a app that takes a chat message string and returns a JSON string with the contents of the chat message.

The content which needs to look for is:
1- Chat  message
2- Prevent any kind of phone number sharing with the following format +CCAABBBBBBB (Min and max length of 11 digits with prefix of “+”)
3- URL in the chat message with the title of the page. Any URL except carlist.my should be redacted.


### How do I get set up? ###

*Install Pods - No pods exist but just for project configuration
* Run the Application on two device or simulator
* Enter the usernames respectively for each device
* Enter receivers username 
* Enter the message and submit.
* To get back the JSON response make sure you enter senders username

### Third Part Library ###

* To implement Instant Messaging I have used SINCH Sdk

### Implementation Details ###
* Implemented business logic in Business Layer
* Implemented View Logic in UI Layer

### Demo Video  ###

https://www.dropbox.com/s/7v909pbm6t5uk26/iCar2.mp4?dl=0


### Example  ###

*********************************************
Chat Message: 
“Hi Lee, can  you call me at +60175570098“
Return (String):
{“message”: [“Hi Lee, can  you call me at  ************”] }

*********************************************
Chat Message: 
“You can find the listing at https://www.carlist.my/used-cars/3300445/2011-toyota-vios-1-5-trd-sportivo-33-000km-full-toyota-serviced-record-like-new-11/“
Return (String):
{“message”: [“You can find the listing at”], “Links”:[{“url”:”https://www.carlist.my/used-cars/3300445/2011-toyota-vios-1-5-trd-sportivo-33-000km-full-toyota-serviced-record-like-new-11/”,”title”:”Toyota Vios 2011 TRD Sportivo 1.5 in Selangor Automatic Sedan Black for RM 49,800 - 3300445 - Carlist.my”}] }


*********************************************  

Chat Message: 
“I have another car at  http://www.example.com/listing10.htm“
 Return (String):
 {“message”: [“i have another another car at *****”]}


